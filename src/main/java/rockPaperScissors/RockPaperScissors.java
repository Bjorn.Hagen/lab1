package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Integer alterimmutablejo = 0;
   

    String tilfeldig() {
        //lage funksjon som velger random mellom rock,paper,scissors
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;
    }
    

    String hvavelgerdu() {
        ///lage en funksjon som spør brukeren hva de velger
        //System.out.println("Your choice (Rock/Paper/Scissors)?");
        String humanchoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        if (humanchoice.equals("rock") ||
            humanchoice.equals("scissors") ||
            humanchoice.equals("paper"));
        else {
            System.out.println("I don't understand " + humanchoice + ".Could you try again?");
        }
        return humanchoice;
        }
    
    void hvemvant(String humanchoice, String randomElement) {
        //lage funksjon som finner ut om spiller eller PC vant runden
        if (humanchoice.equals("paper") && randomElement.equals("rock")) {humanScore = humanScore + 1; 
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". Human wins!");}
        if (humanchoice.equals("scissors") && randomElement.equals("paper")) {humanScore = humanScore + 1;
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". Human wins!");}
        if (humanchoice.equals("rock") && randomElement.equals("scissors")) {humanScore = humanScore + 1;
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". Human wins!");}
        
        if (humanchoice.equals(randomElement)) {
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". It's a tie!");}
        
        if (humanchoice.equals("rock") && randomElement.equals("paper")) {computerScore = computerScore + 1; 
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". Computer wins!");}
        if (humanchoice.equals("paper") && randomElement.equals("scissors")) {computerScore = computerScore + 1;
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". Computer wins!");}
        if (humanchoice.equals("scissors") && randomElement.equals("rock")) {computerScore = computerScore + 1;
            System.out.println("Human chose " + humanchoice + ", computer chose " + randomElement + ". Computer wins!");}
    }

    /*String vilduspillemer() {
        //lage en funksjon som spør om brukeren vil fortsette å spille
        //System.out.println("Do you wish to continue playing? (y/n)?");
        String continueplaying = readInput("Do you wish to continue playing? (y/n)").toLowerCase();
        if (continueplaying.equals("y")) {
            return continueplaying;
        }
        else if (continueplaying.equals("n")) {
            return continueplaying;
        }
        
        else {System.out.println("I don't understand " + continueplaying + ". Could you try again?");}
        return continueplaying;
    }
    */
    public void run() {
        while (alterimmutablejo == 0) {
            System.out.print("Let's play round ");System.out.println(roundCounter);
            
            String mittvalg = hvavelgerdu();
            String CPUvalg = tilfeldig();
            hvemvant(mittvalg,CPUvalg);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            roundCounter = roundCounter + 1;
            while (alterimmutablejo == 0) {
                String continueplaying = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                if (continueplaying.equals("n")) {alterimmutablejo = alterimmutablejo + 1; break;}
                if (continueplaying.equals("y")) {break;}
                
                System.out.println("I don't understand " + continueplaying + ". Could you try again?");}
                
        
        }
        System.out.println("Bye bye :)");
    }
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
